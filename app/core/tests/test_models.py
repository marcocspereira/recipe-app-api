from django.test import TestCase
from django.contrib.auth import get_user_model
from core import models


def sample_user(email="test@recipe.com", password="Test123"):
    """Create a sample user"""
    return get_user_model().objects.create_user(email, password)


class ModelTests(TestCase):

    def test_create_user_with_email_success(self):
        """Test creating a new user with an email is successful"""

        # setup
        email = "test@recipe.com"
        password = "Testpass123"
        user = get_user_model().objects.create_user(
            email=email,
            password=password
        )

        # assertation
        self.assertEqual(user.email, email)
        self.assertTrue(user.check_password(password))

    def test_new_user_email_normalized(self):
        """Test the email for a new user is normalized"""

        # setup
        email = "test@RECIPE.com"
        user = get_user_model().objects.create_user(email, 'test123')

        # assertion
        self.assertEqual(user.email, email.lower())

    def test_new_user_invalid_email(self):
        """Test creating user with no email raises error"""
        with self.assertRaises(ValueError):
            get_user_model().objects.create_user(None, 'test123')

    def test_create_new_superuser(self):
        """Test creating new superuser"""

        # setup
        user = get_user_model().objects.create_superuser(
            'test@recipe.com',
            'test123'
        )

        # assertion
        self.assertTrue(user.is_superuser)
        self.assertTrue(user.is_staff)

    def test_tag_str(self):
        """Test the tag string representation"""
        # setup
        tag = models.Tag.objects.create(
            user=sample_user(),
            name='Vegan'
        )

        # assertion
        self.assertEqual(str(tag), tag.name)
