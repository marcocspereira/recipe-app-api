# allow to mark the behaviour of the "Django get a database" function
# this simulate the database being available and not being available
from unittest.mock import patch
from django.core.management import call_command
# operational error that Django throws when db is unavailable
from django.db.utils import OperationalError
from django.test import TestCase


class CommandTests(TestCase):

    def test_wait_for_db_ready(self):
        """Test waiting for db when db is available"""
        # simulate the behaviour of Django when the database
        # is available

        # setup

        # management command tries and retrieve the database connection
        # try to retrieve the default database via ConnectionHandler
        with patch('django.db.utils.ConnectionHandler.__getitem__') as gi:
            # the way we mock the behaviour of a function
            gi.return_value = True

            # assertion
            call_command('wait_for_db')
            # check __getitem__ was called once
            self.assertEqual(gi.call_count, 1)

    # simulate a little delay
    # the mock replaces the behaviour of time.sleep and replaces it with
    # a mock function and returns True
    # That means during the test it won't actually wait
    @patch('time.sleep', return_value=True)
    def test_wait_for_db(self, ts):
        """Test waiting for db"""

        # setup

        # we try the database 5 times and on th 6th it will
        # be successful and it will continue
        with patch('django.db.utils.ConnectionHandler.__getitem__') as gi:
            # add a side effect to the function we are mocking
            gi.side_effect = [OperationalError] * 5 + [True]

            # assertion
            call_command('wait_for_db')
            self.assertEqual(gi.call_count, 6)
