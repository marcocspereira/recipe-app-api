
# test client that will allow to test requests to the application
from django.test import TestCase, Client
# to get user model
from django.contrib.auth import get_user_model
# helper function called reverse which allows to crete URL
# for Django admin page
from django.urls import reverse


class AdminSiteTests(TestCase):

    # setup function that runs before every test that we run
    def setUp(self):
        self.client = Client()

        self.admin_user = get_user_model().objects.create_superuser(
            email='admin@recipe.com',
            password='Test123'
        )
        # uses Client's helper function that allows to log an user with the
        #  Django authentication
        self.client.force_login(self.admin_user)

        self.user = get_user_model().objects.create_user(
            email='test@recipe.com',
            password='Test123',
            name='Test user full name'
        )

    def test_users_listed(self):
        """Test that users are listed on user page"""

        # setup
        # reverse(app:url)
        url = reverse('admin:core_user_changelist')
        res = self.client.get(url)

        # assertion
        self.assertContains(res, self.user.name)
        self.assertContains(res, self.user.email)

    def test_user_change_page(self):
        """Test that user edit page works"""
        # setup
        url = reverse("admin:core_user_change", args=[self.user.id])
        # /admin/core/user/1 (user id)
        res = self.client.get(url)

        # assertion
        self.assertEqual(res.status_code, 200)

    def test_create_user_page(self):
        """Test that the create user page works"""
        # setup
        url = reverse("admin:core_user_add")
        res = self.client.get(url)

        # assertion
        self.assertEqual(res.status_code, 200)
