import time

# connections module which is what we can use to test db connection
from django.db import connections
# operational error that Django will throw if db isn't available
from django.db.utils import OperationalError
# base command which is the class that we need to build on
# in order to create our custom command
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    """Django command to pause execution until db is available"""

    # what is run whenever we run this management command
    def handle(self, *args, **options):
        # output a message to the screen
        self.stdout.write('Waiting for database....')
        db_conn = None
        while not db_conn:
            try:
                db_conn = connections['default']
            except OperationalError:
                self.stdout.write('Database unvailable, waiting 1 second...')
                time.sleep(1)

        self.stdout.write(self.style.SUCCESS('Database available'))
