from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
# the recommended convention for converting strings in python
# to human readable text
from django.utils.translation import gettext as _

from . import models


class UserAdmin(BaseUserAdmin):

    ordering = ['id']
    list_display = ['email', 'name']

    # update user
    fieldsets = (           # () is a section
        (None, {
            'fields': (
                'email', 'password'
            ),
        }),
        (_('Personal Info'), {
            'fields': (
                'name',
            )
        }),
        (_('Permisions'), {
            'fields': (
                'is_active', 'is_staff', 'is_superuser'
            )
        }),
        (_('Important dates'), {
            'fields': (
                'last_login',
            )
        })
    )

    # create user
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': (
                'email', 'password1', 'password2'
            )
        }),
    )


# register in Django admin
admin.site.register(models.User, UserAdmin)
admin.site.register(models.Tag)
