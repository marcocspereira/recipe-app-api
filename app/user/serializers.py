# we need user model to create our model serializer
from django.contrib.auth import get_user_model, authenticate
# serializers module from the rest framework
from rest_framework import serializers
# whenever you are outputing any messages in the python code
# are going to be output to this screen
from django.utils.translation import ugettext_lazy as _


class UserSerializer(serializers.ModelSerializer):
    """Serializer for the users object"""

    class Meta:
        model = get_user_model()
        # fields to include in serializer to be accessible to the API
        # converted to and from json when we make http post  and then retreive
        fields = ('email', 'password', 'name')
        # configure a few extra settings in model serializer
        # to ensure that the password is right only and the minimum
        # required length is five characters.
        extra_kwargs = {'password': {'write_only': True, 'min_length': 5}}

    # when we're ready to create the user
    # it will cause this create function
    # and it will pass in the validated data
    def create(self, validated_data):
        """Create a new user with encrtypted password and return it"""
        return get_user_model().objects.create_user(**validated_data)

    # the purpose is to make sure that password is setted using Password
    # function instead of just setting it to whichever value is provided
    def update(self, instance, validated_data):
        """Update a user, setting the password correctly and return it"""
        # - instance is going to be the model instance that is linked
        # to our model serialzer (that's going to be our user object)
        # - validated_data is going to be these fields that have been
        # through the validation and ready to update
        password = validated_data.pop('password', None)
        # super will call ModelSerializer's update function
        user = super().update(instance, validated_data)

        if password:
            user.set_password(password)
            user.save()

        return user


class AuthTokenSerializer(serializers.Serializer):
    """Serializer for the user authentication object"""
    email = serializers.CharField()
    password = serializers.CharField(
        style={'input_type': 'password'},
        trim_whitespace=False
    )

    # validation inputs are all corrects
    def validate(self, attrs):
        """Validate and authenticate the user"""
        email = attrs.get('email')
        password = attrs.get('password')

        user = authenticate(
            request=self.context.get('request'),
            username=email,
            password=password
        )
        if not user:
            msg = _('Unable to authenticate with valid credentials')
            raise serializers.ValidationError(msg, code='authentication')

        attrs['user'] = user
        return attrs
