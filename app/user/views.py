# managing our create user API

from rest_framework import generics, authentication, permissions
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.settings import api_settings

from .serializers import UserSerializer, AuthTokenSerializer


class CreateUserView(generics.CreateAPIView):
    """ Create a new user in the system"""
    # this view is a class variable that points to the serializer class
    # that we want to use to create the object
    serializer_class = UserSerializer


class CreateTokenView(ObtainAuthToken):
    """Create a new auth token for user"""
    serializer_class = AuthTokenSerializer
    renderer_classes = api_settings.DEFAULT_RENDERER_CLASSES


class ManageUserView(generics.RetrieveUpdateAPIView):
    """Manage the authenticated user"""
    serializer_class = UserSerializer
    # authentication
    authentication_classes = (authentication.TokenAuthentication,)
    # level of access that the user has
    # (must be authenticated to use the API)
    permission_classes = (permissions.IsAuthenticated,)

    # get object function to API view to get the model for the log in user
    # we're going to override the get objects
    # and return the user that is authenticated
    def get_object(self):
        """Retrieve and return authentication user"""
        # when the object is called, the request will have the
        # user attached to it because of the authentication classes
        # that takes care of taking authenticated user and
        # assigning it to request
        return self.request.user
