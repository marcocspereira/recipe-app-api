# Alpine image is a very lightweight and minimal image that runs python 
FROM python:3.7-alpine

MAINTAINER Marco Pereira

# ------
# set python unbuffered environment variable

# the way we set an environment variable in a docker build file
#   is typing ENV and the environment variable we want to set.

# tells python to run in unbuffered mode which is recomened when
#   running python within Docker containers, because it doesn't
#   allow python to buffer the outputs. It just prints them directly.
ENV PYTHONUNBUFFERED 1

# ------
# install dependencies
# copy from the directory adjacent to the Dockerfile
#   to the Docker iamge
COPY ./requirements.txt /requirements.txt

 # install PostgreSQL client
RUN apk add --update --no-cache postgresql-client
# it uses package manager that comes to alpine add a package
# (update means update the registry before add it)
# (--no-cache means to do not store the registry index on Dockerfile 
#   to minimize the number of extra files and packages that are
#   included in dockerfile container)

# temporary packages that need to be installed in the system while
# we run the requirements and then they can be removed after the
# requirements has run
RUN apk add --update --no-cache --virtual .tmp-build-deps \
        gcc libc-dev linux-headers postgresql-dev

# (--virtual sets up an alias for our dependencies that we can
#   easily remove all those dependencies later)

RUN pip install -r /requirements.txt

RUN apk del .tmp-build-deps

# ------
# make a directory within Docker image that can be used 
#   to store our application source code

# create an empty folder on docker image
RUN mkdir /app
# switches to that as the default directory
# any application we run using our docker conainer, will run
#   starting from this location unless we specify otherwise
WORKDIR /app
# copy from local machine (app folder) to app folder we created on our image
#   it allows to take the code and copy it to our docker image
COPY ./app /app

# ------
# create an user that is going to run our application using docker
#   -D means that is going to be used to running applications only(!),
#   to run processes from our project
RUN adduser -D user
# switch to that user
USER user