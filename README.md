# recipe-app-api
Recipe app API source code.

## docker

### build docker image
* **docker build .**: build which ever _Dockerfile_ is in the root of our project.

* **docker-compose build**: builds our image using docker compose configuration.

* **docker-compose run name_of_the_service `command`**: run commands using docker compose, where `command` is going to be the command that gets run on the linux container. E.g.: `docker-compose run app sh -c "django-admin.py startproject app ."`

* **docker-compose up**: start the services that compose the `docker-compose`.

### docker-compose.yml

* **depends_on**: express dependency between services

## django

* **django-admin.py startproject project_name .**: it creates new project called project_name located in our current location.

* **python manage.py startapp core .**: using `manage.py`helper script, it creates a core app which will hold all of the central code that it is important to the rest of the sub apps that are created in the systme. E.g.: migrations and the database we would like to put this all in the core module

* **python manage.py makemigration name_of_the_app**: it is going to run now database migrations which will create a new migration file, which basically is the instructions for Django create the model in the real database we use.

* **python manage.py createsuperuser**: creates a user with admin role.

## db (postgres)
* install package file that is use for Django to communicate with Docker. In order to do this, we have to add some dependencies to **Dockerfile** during the build process.

    * update `requirements.txt` with `psycopg2`.

    * update `Dockerfile`, before the command to run **pip install requirements.txt**, with:
    ````yml
    # install PostgreSQL client
    RUN apk add --update --no-cache postgresql-client
    # it uses package manager that comes to alpine add a package (update means update the registry before add it) (--no-cache means to do not store the registry index on Dockerfile to minimize the number of extra files and packages that are included in dockerfile container)

    # temporary packages that need to be installed in the system while
    # we run the requirements and then they can be removed after the
    # requirements has run
    RUN apk add --update --no-cache --virtual .tmp-build-deps \
            gcc libc-dev linux-headers postgresql-dev
    # (--virtual sets up an alias for our dependencies that we can
    #   easily remove all those dependencies later)
    ````

    * then run `docker-compose build`

## Mocking

* Change behaviour of dependencies

* We implement a management command to the core app of Django project, that is a helper command that allows to wait for the database to be available before continue to in and running other commands.

* This command is used in `docker-compose.yml` file when the Django app is starting.

* The **reason** that we need this command is because that sometimes, when using Postgres with docker-compose in a Django app, sometimes Django fails to start because of a database error.

    * This is because once the Postgres servies started there were a few extra set up tasks that need to be done on the Postgres before is ready to accept connections.

    * This means a Django app will try and connect to database before database is ready and therefore will fail with an exception an we need to restart the app.

    * To improve the reliability of products, we add this helper command that we can put in front of all of the commands we run in docker-compose, and that will ensure that a database is up and ready to accept connections, before we try to access the database.

    * The command is `wait_for_db`.

    * [Django unit test wait for database](https://stackoverflow.com/questions/52621819/django-unit-test-wait-for-database)